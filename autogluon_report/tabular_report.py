from .report import BaseReport
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


class TabularReport(BaseReport):
    def generate(self, force=False, components=[]):
        if len(self.blocks) > 1 and not force:
            return
        super().generate(force=force, components=components)
        for component in components if len(components) > 0 else self.components:
            if component == "column_impact":
                self.generate_column_impact()
            elif component == "scoring":
                self.generate_scoring()

    def generate_column_impact(self):
        for column_name in self.predictor.feature_metadata_in.get_features():
            if pd.api.types.is_numeric_dtype(self.test_data[column_name]):
                self.generate_column_impact_plot(column_name)

    def generate_scoring(self, target_column=None):
        if target_column is None:
            target_column = self.predictor.label
        rmse = np.sqrt(np.mean((self.predictions - self.test_data[target_column]) ** 2))
        fig, ax = plt.subplots()
        ax.scatter(self.predictions, self.test_data[target_column])
        ax.set_xlabel(f"Predicted values of {target_column}")
        ax.set_ylabel(f"Actual values of {target_column}")
        ax.set_title(f"Predicted vs Actual (RMSE: {rmse})")
        # Plotting the RMSE range
        ax.fill_between(
            self.predictions,
            self.test_data[target_column] - rmse,
            self.test_data[target_column] + rmse,
            color="b",
            alpha=0.2,
        )
        plt.savefig(f"{self.assets_path}/predicted_vs_actual_{target_column}.png")
        plt.close(fig)
        self.blocks.append(
            (
                f"<br>"
                f'<div style="text-align:center;">'
                f'<img src="assets/predicted_vs_actual_{target_column}.png" alt="Predicted vs Actual">'
                f"</div>"
                f"<p>This graph represents the predicted vs actual values of the '{target_column}'. The Root Mean Square Error (RMSE) is also calculated to measure the differences between values predicted by the model and the actual values. The shaded area represents the RMSE range.</p>"
            )
        )

    def generate_column_impact_plot(self, column_name: str):
        fig, ax = plt.subplots()
        ax.scatter(self.test_data[column_name], self.predictions)
        ax.set_xlabel(column_name)
        ax.set_ylabel("Impact on prediction")
        ax.set_title("Column Impact on Prediction")
        plt.savefig(f"{self.assets_path}/{column_name}_impact.png")
        self.blocks.append(
            (
                f"<br>"
                f'<div style="text-align:center;">'
                f'<img src="assets/{column_name}_impact.png" alt="Column Impact">'
                f"</div>"
                f"<p>This graph represents the impact of the column '{column_name}' on the prediction. It shows how changes in the column's values can affect the prediction.</p>"
            )
        )
        plt.close(fig)
