from .tabular_report import TabularReport
from autogluon import tabular
from typing import List, Optional


class Report:
    report_implementation = None

    def __init__(
        self,
        predictor,
        test_data,
        predictions,
        path="reports",
        name="report",
        components=[
            "leaderboard",
            "feature_importance",
            "confusion_matrix",
            "column_impact",
            "scoring",
        ],
        leaderboard=None,
        feature_importance=None,
    ):
        if isinstance(predictor, tabular.predictor.predictor.TabularPredictor):
            self.report_implementation = TabularReport(
                predictor,
                test_data,
                predictions,
                path=path,
                name=name,
                components=components,
                leaderboard=leaderboard,
                feature_importance=feature_importance,
            )

    def generate(self, force=False, components: Optional[List[str]] = []):
        if not self.report_implementation:
            return None
        return self.report_implementation.generate(force=force, components=components)

    def save_html(self, path: Optional[str] = None, name: Optional[str] = None):
        if not self.report_implementation:
            return None
        return self.report_implementation.save_html(path=path, name=name)

    def save_pdf(self, path: Optional[str] = None, name: Optional[str] = None):
        if not self.report_implementation:
            return None
        return self.report_implementation.save_pdf(path=path, name=name)

    def save(
        self,
        formats=["html", "pdf"],
        path: Optional[str] = None,
        name: Optional[str] = None,
    ):
        if not self.report_implementation:
            return None
        return self.report_implementation.save(formats=formats, path=path, name=name)
