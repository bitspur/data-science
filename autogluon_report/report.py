from weasyprint import HTML, CSS
import numpy as np
from bs4 import BeautifulSoup
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import os
import seaborn as sns
from typing import List, Optional


class BaseReport:
    blocks = [
        """<style>
table {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-size: 0.58em;
    border-collapse: collapse;
    color: #333;
    background: #fafafa;
    margin-left: auto;
    margin-right: auto;
}
tr {
    break-inside: avoid;
    border-bottom: 1px solid #ccc;
}
th, td {
    border: 1px solid #ddd;
    padding: 3px;
    padding-top: 4px;
    padding-bottom: 4px;
    white-space: wrap;
    overflow: auto;
    text-overflow: ellipsis;
}
tr:nth-child(even) {
    background: #f3f3f3;
}
th {
    padding-top: 6px;
    padding-bottom: 6px;
    text-align: left;
}
thead th {
    background-color: #e9e9e9;
    color: #333;
}
    </style>"""
    ]
    color_map: mcolors.LinearSegmentedColormap = (
        mcolors.LinearSegmentedColormap.from_list("n", ["#6495ed", "#ff7f7f"])
    )

    def __init__(
        self,
        predictor,
        test_data,
        predictions,
        path="reports",
        name="report",
        components=[
            "leaderboard",
            "feature_importance",
            "confusion_matrix",
        ],
        leaderboard=None,
        feature_importance=None,
    ):
        self.components = components
        self.name = name
        self.predictions = predictions
        self.predictor = predictor
        self.test_data = test_data
        self.path = path
        self.leaderboard = leaderboard
        self.feature_importance = feature_importance
        self.assets_path = os.path.join(self.path, "assets")

    def generate(self, force=False, components=[]):
        if len(self.blocks) > 1 and not force:
            return
        if not os.path.exists(self.assets_path):
            os.makedirs(self.assets_path)
        for component in components if len(components) > 0 else self.components:
            if component == "leaderboard":
                self.generate_leaderboard()
            elif component == "feature_importance":
                self.generate_feature_importance()
            elif component == "confusion_matrix":
                self.generate_confusion_matrix()

    def generate_leaderboard(self):
        leaderboard = (
            self.leaderboard
            if self.leaderboard is not None
            else self.predictor.leaderboard(silent=True)
        )
        self.generate_leaderboard_table(leaderboard)
        self.generate_score_val_plot(leaderboard)
        self.generate_fit_time_plot(leaderboard)
        self.generate_pred_time_val_plot(leaderboard)
        self.generate_score_val_vs_fit_time_plot(leaderboard)
        self.generate_score_val_vs_pred_time_val_plot(leaderboard)

    def generate_leaderboard_table(self, leaderboard):
        leaderboard_html = leaderboard.to_html(border=1)
        soup = BeautifulSoup(leaderboard_html, "html.parser")
        for row in soup.find_all("tr"):
            first_column = row.find("th") or row.find("td")
            if first_column:
                first_column.decompose()
            second_column = row.find("th") or row.find("td")
            if second_column:
                second_column[
                    "style"
                ] = "padding-top: 6px; padding-bottom: 6px; text-align: left; font-weight: bold;"
        leaderboard_html = str(soup)
        self.blocks.append(leaderboard_html)

    def generate_score_val_plot(self, leaderboard):
        leaderboard["score_val_norm"] = (
            leaderboard["score_val"] - leaderboard["score_val"].min()
        ) / (leaderboard["score_val"].max() - leaderboard["score_val"].min())
        ax = leaderboard.plot(
            x="model",
            y="score_val",
            kind="barh",
            title="score_val",
            color=self.color_map(leaderboard["score_val_norm"]),
        )
        ax.get_legend().remove()
        plt.tight_layout()
        plt.savefig(os.path.join(self.assets_path, "leaderboard_score_val.png"))
        self.blocks.append(
            (
                f"<br>"
                f'<div style="text-align:center;">'
                f'<img src="assets/leaderboard_score_val.png" alt="Leaderboard (score_val)">'
                f"</div>"
                f"<p>The graph represents the score value for each model, indicating the model's performance. "
                f"A higher score value signifies a more accurate model on the validation set. "
                f"The score value is calculated based on the {self.predictor.eval_metric} used for the task.</p>"
            )
        )
        plt.clf()

    def generate_fit_time_plot(self, leaderboard):
        leaderboard["fit_time_norm"] = (
            leaderboard["fit_time"] - leaderboard["fit_time"].min()
        ) / (leaderboard["fit_time"].max() - leaderboard["fit_time"].min())
        ax = leaderboard.plot(
            x="model",
            y="fit_time",
            kind="barh",
            title="fit_time",
            color=self.color_map(leaderboard["fit_time_norm"]),
        )
        ax.get_legend().remove()
        plt.tight_layout()
        plt.savefig(os.path.join(self.assets_path, "leaderboard_fit_time.png"))
        self.blocks.append(
            (
                f"<br>"
                f'<div style="text-align:center;">'
                f'<img src="assets/leaderboard_fit_time.png" alt="Leaderboard (fit_time)">'
                f"</div>"
                f"<p>This graph represents the fit time for each model, which is the time taken by the model to train on the given dataset. It is an important metric to consider as models with longer fit times may not be practical for use in real-time applications, despite potentially offering higher accuracy.</p>"
            )
        )
        plt.clf()

    def generate_pred_time_val_plot(self, leaderboard):
        leaderboard["pred_time_val_norm"] = (
            leaderboard["pred_time_val"] - leaderboard["pred_time_val"].min()
        ) / (leaderboard["pred_time_val"].max() - leaderboard["pred_time_val"].min())
        ax = leaderboard.plot(
            x="model",
            y="pred_time_val",
            kind="barh",
            title="pred_time_val",
            color=self.color_map(leaderboard["pred_time_val_norm"]),
        )
        ax.get_legend().remove()
        plt.tight_layout()
        plt.savefig(os.path.join(self.assets_path, "leaderboard_pred_time_val.png"))
        self.blocks.append(
            (
                f"<br>"
                f'<div style="text-align:center;">'
                f'<img src="assets/leaderboard_pred_time_val.png" alt="Leaderboard (pred_time_val)">'
                f"</div>"
                f"<p>This graph represents the prediction time value for each model.</p>"
            )
        )
        plt.clf()

    def generate_score_val_vs_fit_time_plot(self, leaderboard):
        leaderboard["score_val_norm"] = (
            leaderboard["score_val"] - leaderboard["score_val"].min()
        ) / (leaderboard["score_val"].max() - leaderboard["score_val"].min())
        colors = self.color_map(leaderboard["score_val_norm"])
        markers = [
            "o",
            "v",
            "^",
            "<",
            ">",
            "8",
            "s",
            "p",
            "*",
            "h",
            "H",
            "D",
            "d",
            "+",
            "x",
        ]
        for i, model in enumerate(leaderboard["model"]):
            plt.scatter(
                leaderboard.loc[leaderboard["model"] == model, "fit_time"],
                leaderboard.loc[leaderboard["model"] == model, "score_val"],
                alpha=0.5,
                c=colors[i],
                marker=markers[i % len(markers)],
                s=100,
            )
        plt.title("Score Value vs Fit Time")
        plt.xlabel("Fit Time")
        plt.ylabel("Score Value")
        plt.legend(leaderboard["model"], title="Models")
        plt.tight_layout()
        plt.savefig(os.path.join(self.assets_path, "score_val_vs_fit_time.png"))
        self.blocks.append(
            (
                f"<br>"
                f'<div style="text-align:center;">'
                f'<img src="assets/score_val_vs_fit_time.png" alt="Score Value vs Fit Time">'
                f"</div>"
                f"<p>This graph represents the score value against fit time for each model.</p>"
            )
        )
        plt.clf()

    def generate_score_val_vs_pred_time_val_plot(self, leaderboard):
        leaderboard["score_val_norm"] = (
            leaderboard["score_val"] - leaderboard["score_val"].min()
        ) / (leaderboard["score_val"].max() - leaderboard["score_val"].min())
        colors = self.color_map(leaderboard["score_val_norm"])
        markers = [
            "o",
            "v",
            "^",
            "<",
            ">",
            "8",
            "s",
            "p",
            "*",
            "h",
            "H",
            "D",
            "d",
            "+",
            "x",
        ]
        for i, model in enumerate(leaderboard["model"]):
            plt.scatter(
                leaderboard.loc[leaderboard["model"] == model, "pred_time_val"],
                leaderboard.loc[leaderboard["model"] == model, "score_val"],
                alpha=0.5,
                c=colors[i],
                marker=markers[i % len(markers)],
                s=100,
            )
        plt.title("Score Value vs Prediction Time")
        plt.xlabel("Prediction Time")
        plt.ylabel("Score Value")
        plt.legend(leaderboard["model"], title="Models")
        plt.tight_layout()
        plt.savefig(os.path.join(self.assets_path, "score_val_vs_pred_time_val.png"))
        self.blocks.append(
            (
                f"<br>"
                f'<div style="text-align:center;">'
                f'<img src="assets/score_val_vs_pred_time_val.png" alt="Score Value vs Prediction Time">'
                f"</div>"
                f"<p>This graph represents the score value against prediction time for each model.</p>"
            )
        )
        plt.clf()

    def generate_feature_importance(self):
        feature_importance = (
            self.feature_importance
            if self.feature_importance is not None
            else self.predictor.feature_importance(self.test_data, silent=True)
        )
        self.generate_feature_importance_table(feature_importance)
        self.generate_feature_importance_plots(feature_importance)

    def generate_feature_importance_table(self, feature_importance):
        self.blocks.append(feature_importance.to_html(border=1))

    def generate_feature_importance_plots(self, feature_importance):
        metrics = feature_importance.columns
        for metric in metrics:
            feature_importance[metric].plot(kind="barh", title=metric)
            plt.tight_layout()
            plt.savefig(
                os.path.join(self.assets_path, f"feature_importance_{metric}.png")
            )
            self.blocks.append(
                (
                    f"<br>"
                    f'<div style="text-align:center;">'
                    f'<img src="assets/feature_importance_{metric}.png" alt="Feature Importance ({metric})">'
                    f"</div>"
                    f"<p>This graph represents the feature importance for the metric {metric}.</p>"
                )
            )
            plt.clf()

    def generate_confusion_matrix(self):
        pass

    def save_html(self, path=None, name=None):
        self.generate(force=False)
        path = path if path else self.path
        name = name if name else self.name
        with open(os.path.join(path, f"{name}.html"), "w") as f:
            for html in self.blocks:
                f.write(html)

    def save_pdf(self, path=None, name=None):
        self.generate(force=False)
        path = path if path else self.path
        name = name if name else self.name
        html_string = "".join(self.blocks)
        HTML(
            string=html_string,
            base_url=self.path,
            media_type="print",
        ).write_pdf(
            os.path.join(path, f"{name}.pdf"),
            stylesheets=[CSS(string="@page { margin: 6px; }")],
        )

    def save(
        self,
        formats=["html", "pdf"],
        path=None,
        name=None,
    ):
        for format in formats:
            if format == "html":
                self.save_html(path=path, name=name)
            elif format == "pdf":
                self.save_pdf(path=path, name=name)
