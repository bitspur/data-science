from autogluon.tabular import TabularDataset, TabularPredictor
from autogluon_report import Report
from pydantic import BaseModel
from sklearn.model_selection import train_test_split
import argparse
import logging
import os
import re
import yaml

logging.basicConfig(
    level=getattr(
        logging,
        os.getenv("LOG_LEVEL", "DEBUG" if os.getenv("DEBUG") == "1" else "INFO"),
    )
)


class ArgsModel(BaseModel):
    channel_config: str = None
    channel_test: str = None
    channel_train: str
    model_dir: str = None
    num_gpus: str
    output_data_dir: str


def main(args: ArgsModel):
    logger = logging.getLogger(__name__)
    logger.info("ARGS" + str(vars(args)))
    os.makedirs(args.output_data_dir, mode=0o777, exist_ok=True)
    config = {}
    if args.channel_config:
        with open(get_input_path(args.channel_config)) as f:
            config = yaml.safe_load(f)
    logger.info("CONFIG" + str(config))
    ag_predictor_args = config.get("ag_predictor_args", {})
    ag_fit_args = config.get("ag_fit_args", {})
    train_test_split_args = config.get("train_test_split_args")
    model_dir = args.model_dir if args.model_dir else args.output_data_dir + "/model"
    train_data = TabularDataset(get_input_path(args.channel_train))
    test_data: TabularDataset = None
    if args.channel_test:
        test_data = TabularDataset(get_input_path(args.channel_test))
    elif train_test_split_args:
        train_data, test_data = train_test_split(
            train_data,
            **{
                "test_size": 0.2,
                **train_test_split_args,
            },
        )
    predictor = TabularPredictor.load(model_dir)
    # predictor = TabularPredictor(**{**ag_predictor_args, "path": model_dir}).fit(
    #     train_data, **ag_fit_args
    # )
    if test_data is not None:
        y_predictions = predictor.predict(test_data)
        if config.get("output_prediction_format", "csv") == "parquet":
            y_predictions.to_parquet(
                os.path.join(args.output_data_dir, "predictions.parquet")
            )
        else:
            y_predictions.to_csv(os.path.join(args.output_data_dir, "predictions.csv"))
        if config.get("leaderboard", False):
            leaderboard = predictor.leaderboard(test_data, silent=False)
            leaderboard.to_csv(os.path.join(args.output_data_dir, "leaderboard.csv"))
        if config.get("feature_importance", False):
            feature_importance = predictor.feature_importance(test_data)
            feature_importance.to_csv(
                os.path.join(args.output_data_dir, "feature_importance.csv")
            )
        report = Report(
            predictor,
            test_data,
            y_predictions,
            leaderboard=leaderboard,
            feature_importance=feature_importance,
            path=os.path.join(args.output_data_dir, "reports"),
        )
        report.save()
    else:
        if config.get("leaderboard", False):
            lb = predictor.leaderboard(silent=False)
            lb.to_csv(os.path.join(args.output_data_dir, "leaderboard.csv"))


def get_input_path(channel: str) -> str:
    if re.match(r"^\w+://", channel):
        channel = channel[7:] if channel.startswith("file://") else channel
    elif os.path.isfile(channel) or os.path.isdir(channel):
        files = os.listdir(channel) if os.path.isdir(channel) else [channel]
        if len(files) > 1:
            logging.warning(f"more than one file is found in {channel} directory")
        channel = os.path.join(channel, files[0]) if os.path.isdir(channel) else channel
    else:
        if not os.path.exists(channel):
            raise Exception(f"{channel} not found")
        if not os.listdir(channel):
            raise Exception(f"no files found in the {channel} directory")
    logging.info(f"using {channel}")
    return channel


def get_env(name, default=None):
    return os.environ.get(f"SM_{name}", os.environ.get(name, default))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output-data-dir",
        type=str,
        default=get_env("OUTPUT_DATA_DIR", os.path.join(os.getcwd(), "output")),
    )
    parser.add_argument(
        "--model-dir",
        type=str,
        default=get_env("MODEL_DIR"),
    )
    parser.add_argument("--num-gpus", type=str, default=str(os.cpu_count()))
    parser.add_argument(
        "--channel-train",
        type=str,
        default=get_env("CHANNEL_TRAIN"),
    )
    parser.add_argument(
        "--channel-test",
        type=str,
        default=get_env("CHANNEL_TEST"),
    )
    parser.add_argument("--channel-config", type=str, default=get_env("CHANNEL_CONFIG"))
    args, _ = parser.parse_known_args()
    main(ArgsModel(**vars(args)))
