from autogluon.core.utils.loaders import load_pd
import torch
from autogluon.multimodal import MultiModalPredictor
import argparse
import os
import yaml


def main(args):
    os.makedirs(args.output_data_dir, mode=0o777, exist_ok=True)
    config_file = get_input_path(args.ag_config)
    with open(config_file) as f:
        config = yaml.safe_load(f)
    if args.num_gpus:
        config["num_gpus"] = int(args.n_gpus)
    train_file = get_input_path(args.training_dir)
    train_data = load_pd.load(train_file)
    ag_predictor_args = config["ag_predictor_args"]
    ag_predictor_args["path"] = args.model_dir
    ag_fit_args = config["ag_fit_args"]
    predictor = MultiModalPredictor(problem_type="ner", label="entity_annotations").fit(
        train_data, **ag_fit_args
    )
    if args.test_dir:
        test_file = get_input_path(args.test_dir)
        test_data = load_pd.load(test_file)
        y_pred = predictor.predict(test_data)
        if config.get("output_prediction_format", "csv") == "parquet":
            y_pred.to_parquet(os.path.join(args.output_data_dir, "predictions.parquet"))
        else:
            y_pred.to_csv(os.path.join(args.output_data_dir, "predictions.csv"))
        if config.get("leaderboard", False):
            lb = predictor.leaderboard(test_data, silent=False)
            lb.to_csv(os.path.join(args.output_data_dir, "leaderboard.csv"))
        if config.get("feature_importance", False):
            feature_importance = predictor.feature_importance(test_data)
            feature_importance.to_csv(
                os.path.join(args.output_data_dir, "feature_importance.csv")
            )
    else:
        if config.get("leaderboard", False):
            lb = predictor.leaderboard(silent=False)
            lb.to_csv(os.path.join(args.output_data_dir, "leaderboard.csv"))


def get_input_path(path):
    files = os.listdir(path)
    file = files[0]
    filename = os.path.join(path, file)
    return filename


def get_env(name, default=None):
    return os.environ.get(f"SM_{name}", os.environ.get(name, default))


if __name__ == "__main__":
    os.environ["MXNET_CUDNN_AUTOTUNE_DEFAULT"] = "0"
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output-data-dir",
        type=str,
        default=get_env("OUTPUT_DATA_DIR", os.path.join(os.getcwd(), "output")),
    )
    parser.add_argument(
        "--model-dir",
        type=str,
        default=get_env("MODEL_DIR", os.path.join(os.getcwd(), "model")),
    )
    parser.add_argument("--n-gpus", type=str, default=str(torch.cuda.device_count()))
    parser.add_argument(
        "--training-dir",
        type=str,
        default=get_env("CHANNEL_TRAIN", os.path.join(os.getcwd(), "train")),
    )
    parser.add_argument(
        "--test-dir",
        type=str,
        default=get_env("CHANNEL_TEST"),
    )
    parser.add_argument(
        "--ag-config", type=str, default=get_env("CHANNEL_CONFIG", "config")
    )
    args, _ = parser.parse_known_args()
    main(args)
