from autogluon.core.utils.loaders import load_pd
from autogluon.multimodal import MultiModalPredictor
from report import Report

data_root = "https://autogluon-text.s3-accelerate.amazonaws.com/glue/sst/"
train_data = load_pd.load(data_root + "train.parquet")
test_data = load_pd.load(data_root + "dev.parquet")
predictor = MultiModalPredictor(label="label").fit(train_data=train_data)

predictions = predictor.predict(test_data)
report = Report(predictor, test_data, predictions)
report.render()
